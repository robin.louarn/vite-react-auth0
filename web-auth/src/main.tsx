import { ChakraProvider } from "@chakra-ui/react";
import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import AuthContext from "./components/AuthContext";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <ChakraProvider>
      <AuthContext.AuthProvider value={AuthContext.webAuth}>
        <App />
      </AuthContext.AuthProvider>
    </ChakraProvider>
  </React.StrictMode>
);
