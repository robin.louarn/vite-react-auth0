import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  useToast,
} from "@chakra-ui/react";
import { Auth0Error } from "auth0-js";
import { ChangeEventHandler, FC, FormEventHandler, useState } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../components/AuthContext";

const Login: FC = () => {
  var [username, setUsername] = useState("");
  var [password, setPassword] = useState("");
  const toast = useToast();

  const webAuth = AuthContext.useAuthContext();
  const databaseConnection = "Username-Password-Authentication";

  const login: FormEventHandler<HTMLDivElement> &
    FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    webAuth.login(
      {
        realm: databaseConnection,
        username: username,
        password: password,
      },
      (err) => {
        if (err) displayError(err);
      }
    );
  };

  const handleEmailChange: ChangeEventHandler<HTMLInputElement> = (e) =>
    setUsername(e.target.value);

  const handlePasswordChange: ChangeEventHandler<HTMLInputElement> = (e) =>
    setPassword(e.target.value);

  const displayError = (err: Auth0Error): void => {
    toast({
      title: err.name,
      description: err.description,
      status: "error",
      duration: 9000,
      isClosable: true,
    });
  };

  return (
    <Flex
      justifyContent="center"
      alignItems="center"
      h="100vh"
      w="100vw"
      flexDir="column"
    >
      <Box
        as="form"
        onSubmit={login}
        p={5}
        shadow="md"
        borderRadius="md"
        backgroundColor="#FFF"
      >
        <FormControl mb={5}>
          <FormLabel>Email address</FormLabel>
          <Input
            type="email"
            placeholder="Enter your email"
            value={username}
            onChange={handleEmailChange}
          />
        </FormControl>
        <FormControl mb={3}>
          <FormLabel>Password</FormLabel>
          <Input
            type="password"
            placeholder="Enter your email"
            value={password}
            onChange={handlePasswordChange}
          />
        </FormControl>
        <Button as={Link} mb={5} variant="link" to="/forgot_password">
          Forgot password
        </Button>
        <Flex justifyContent="flex-end">
          <Button as={Link} mr={5} variant="ghost" to="/signup">
            Create account
          </Button>
          <Button type="submit">Log In</Button>
        </Flex>
      </Box>
    </Flex>
  );
};

export default Login;
