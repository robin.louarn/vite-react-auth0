import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  useToast,
} from "@chakra-ui/react";
import { Auth0Error } from "auth0-js";
import { ChangeEventHandler, FC, FormEventHandler, useState } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../components/AuthContext";

const Login: FC = () => {
  var [username, setUsername] = useState("");
  var [password, setPassword] = useState("");
  const toast = useToast();

  const webAuth = AuthContext.useAuthContext();
  const databaseConnection = "Username-Password-Authentication";

  const signup: FormEventHandler<HTMLDivElement> &
    FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    webAuth.redirect.signupAndLogin(
      {
        email: username,
        password,
        connection: databaseConnection,
      },
      (err) => {
        if (err) displayError(err);
      }
    );
  };

  const handleEmailChange: ChangeEventHandler<HTMLInputElement> = (e) =>
    setUsername(e.target.value);

  const handlePasswordChange: ChangeEventHandler<HTMLInputElement> = (e) =>
    setPassword(e.target.value);

  const displayError = (err: Auth0Error): void => {
    toast({
      title: err.name,
      description: JSON.stringify(err.description),
      status: "error",
      duration: 9000,
      isClosable: true,
    });
  };

  return (
    <Flex
      justifyContent="center"
      alignItems="center"
      h="100vh"
      w="100vw"
      flexDir="column"
    >
      <Box
        as="form"
        onSubmit={signup}
        p={5}
        shadow="md"
        borderRadius="md"
        backgroundColor="#FFF"
      >
        <FormControl mb={5}>
          <FormLabel>Email address</FormLabel>
          <Input
            type="email"
            placeholder="Enter your email"
            value={username}
            onChange={handleEmailChange}
          />
        </FormControl>
        <FormControl mb={5}>
          <FormLabel>Password</FormLabel>
          <Input
            type="password"
            placeholder="Enter your email"
            value={password}
            onChange={handlePasswordChange}
          />
        </FormControl>
        <Flex justifyContent="flex-end">
          <Button as={Link} mr={5} variant="gost" to="/login">
            Cancel
          </Button>
          <Button type="submit">Signup</Button>
        </Flex>
      </Box>
    </Flex>
  );
};

export default Login;
