import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  useToast,
} from "@chakra-ui/react";
import { Auth0Error } from "auth0-js";
import { ChangeEventHandler, FC, FormEventHandler, useState } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../components/AuthContext";

const Login: FC = () => {
  var [username, setUsername] = useState("");
  const toast = useToast();

  const webAuth = AuthContext.useAuthContext();
  const databaseConnection = "Username-Password-Authentication";

  const forgotPassword: FormEventHandler<HTMLDivElement> &
    FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    webAuth.changePassword(
      {
        email: username,
        connection: databaseConnection,
      },
      (err) => {
        if (err) {
          displayError(err);
        } else {
          toast({
            title: "reset password",
            description: "an email will be sent",
            status: "success",
            duration: 9000,
            isClosable: true,
          });
        }
      }
    );
  };

  const handleEmailChange: ChangeEventHandler<HTMLInputElement> = (e) =>
    setUsername(e.target.value);

  const displayError = (err: Auth0Error): void => {
    toast({
      title: err.name,
      description: err.description,
      status: "error",
      duration: 9000,
      isClosable: true,
    });
  };

  return (
    <Flex
      justifyContent="center"
      alignItems="center"
      h="100vh"
      w="100vw"
      flexDir="column"
    >
      <Box
        as="form"
        onSubmit={forgotPassword}
        p={5}
        shadow="md"
        borderRadius="md"
        backgroundColor="#FFF"
      >
        <FormControl mb={5}>
          <FormLabel>Email address</FormLabel>
          <Input
            type="email"
            placeholder="Enter your email"
            value={username}
            onChange={handleEmailChange}
          />
        </FormControl>
        <Flex justifyContent="flex-end">
          <Button as={Link} mr={5} variant="gost" to="/login">
            Cancel
          </Button>
          <Button type="submit">Ask reset</Button>
        </Flex>
      </Box>
    </Flex>
  );
};

export default Login;
