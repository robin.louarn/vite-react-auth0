import { chakra, HTMLChakraProps } from "@chakra-ui/react";
import { FC } from "react";

export type SplashScreenProps = {
  isAnimated?: boolean;
} & HTMLChakraProps<"svg">;

export const SplashScreen: FC<SplashScreenProps> = ({
  isAnimated,
  ...props
}) => (
  <chakra.svg
    viewBox="0 0 16 12"
    xmlns="http://www.w3.org/2000/svg"
    preserveAspectRatio="xMidYMid slice"
    position="absolute"
    width="100%"
    height="100%"
    zIndex="hide"
    {...props}
  >
    <rect
      x="10.5"
      y="1"
      width="8"
      height="8"
      rx="1.25"
      fill="#51DDD3"
      transform="rotate(-45 14.5 5)"
    >
      {isAnimated && (
        <animateTransform
          attributeName="transform"
          attributeType="XML"
          type="rotate"
          from="0 14.5 5"
          to="-45 14.5 5"
          dur="500ms"
          repeatCount="1"
          fill="freeze"
          values="0 14.5 5; -48 14.5 5; -45 14.5 5"
          keyTimes="0; 0.65; 1"
        />
      )}
    </rect>
    <rect
      x="5"
      y="-3"
      width="6"
      height="6"
      rx="1.25"
      fill="#79B7EE"
      transform="rotate(-45 8 0)"
    >
      {isAnimated && (
        <animateTransform
          attributeName="transform"
          attributeType="XML"
          type="rotate"
          from="0 8 0"
          to="-45 8 0"
          dur="500ms"
          repeatCount="1"
          fill="freeze"
          values="0 8 0; -48 8 0; -45 8 0"
          keyTimes="0; 0.65; 1"
        />
      )}
    </rect>
    <rect
      x="-2"
      y="0.5"
      width="6"
      height="6"
      rx="1.25"
      fill="#0D3C61"
      transform="rotate(-45 1 3.5)"
    >
      {isAnimated && (
        <animateTransform
          attributeName="transform"
          attributeType="XML"
          type="rotate"
          from="0 1 3.5"
          to="-45 1 3.5"
          dur="500ms"
          repeatCount="1"
          fill="freeze"
          values="0 1 3.5; -48 1 3.5; -45 1 3.5"
          keyTimes="0; 0.65; 1"
        />
      )}
    </rect>
    <rect
      x="-2"
      y="7.5"
      width="11"
      height="4"
      rx="1.25"
      fill="#CAE3EB"
      transform="rotate(-45 3.5 9.5)"
    >
      {isAnimated && (
        <animateTransform
          attributeName="transform"
          attributeType="XML"
          type="rotate"
          from="0 3.5 9.5"
          to="-45 3.5 9.5"
          dur="500ms"
          repeatCount="1"
          fill="freeze"
          values="0 3.5 9.5; -48 3.5 9.5; -45 3.5 9.5"
          keyTimes="0; 0.65; 1"
        />
      )}
    </rect>
    <rect
      x="7.5"
      y="9.25"
      width="5"
      height="5"
      rx="1.25"
      fill="#79B7EE"
      transform="rotate(-45 10 11.75)"
    >
      {isAnimated && (
        <animateTransform
          attributeName="transform"
          attributeType="XML"
          type="rotate"
          from="0 10 11.75"
          to="-45 10 11.75"
          dur="500ms"
          repeatCount="1"
          fill="freeze"
          values="0 10 11.75; -48 10 11.75; -45 10 11.75"
          keyTimes="0; 0.65; 1"
        />
      )}
    </rect>
  </chakra.svg>
);
