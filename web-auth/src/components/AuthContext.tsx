import { WebAuth } from "auth0-js";
import { createContext, useContext } from "react";

const config =
  process.env.NODE_ENV === "production"
    ? JSON.parse(decodeURIComponent(escape(window.atob("@@config@@"))))
    : {
        auth0Tenant: "",
        authorizationServer: {
          issuer: "",
        },
        auth0Domain: "",
        clientID: "",
        callbackURL: "",
        internalOptions: {},
      };

var params = Object.assign(
  {
    overrides: {
      __tenant: config.auth0Tenant,
      __token_issuer: config.authorizationServer.issuer,
    },
    domain: config.auth0Domain,
    clientID: config.clientID,
    redirectUri: config.callbackURL,
    responseType: "code",
  },
  config.internalOptions
);

const webAuth = new WebAuth(params);

const authContext = createContext<WebAuth | undefined>(undefined);

const useAuthContext = (): WebAuth => {
  const context = useContext(authContext);

  if (context) {
    return context;
  } else {
    throw new Error("AuthContext is undefined");
  }
};
export default {
  AuthProvider: authContext.Provider,
  useAuthContext,
  webAuth,
};
