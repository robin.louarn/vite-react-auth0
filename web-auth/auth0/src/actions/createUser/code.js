const axios = require("axios");

/**
 * Handler that will be called during the execution of a PostUserRegistration flow.
 *
 * @param {Event} event - Details about the context and user that has registered.
 */
exports.onExecutePostUserRegistration = async (event) => {
  await axios.post(
    "https://webhook.site/e128abda-638f-4088-ad5d-4515465728fb",
    { type: "createProject", event, tenant_name: "##AUTH0_TENANT_NAME##" }
  );
};
