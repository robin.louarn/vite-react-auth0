import { useAuth0 } from "@auth0/auth0-react";
import { Flex } from "@chakra-ui/react";
import { FC } from "react";
import LoginButton from "./components/LoginButton";
import LogoutButton from "./components/LogoutButton";
import Profile from "./components/Profile";

const App: FC = () => {
  const { isAuthenticated } = useAuth0();

  return (
    <Flex
      justifyContent="center"
      alignItems="center"
      h="100vh"
      w="100vw"
      flexDir="column"
    >
      <Flex justifyContent="center" alignContent="center" p={5}>
        {!isAuthenticated ? <LoginButton /> : <LogoutButton />}
      </Flex>
      <Profile />
    </Flex>
  );
};

export default App;
