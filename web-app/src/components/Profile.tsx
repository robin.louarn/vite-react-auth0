import { useAuth0 } from "@auth0/auth0-react";
import { Box, Flex, Heading, Image, Text } from "@chakra-ui/react";
import { FC } from "react";

const Profile: FC = () => {
  const { user, isAuthenticated, isLoading } = useAuth0();

  if (isLoading) {
    return <div>Loading ...</div>;
  }

  return (
    <>
      {isAuthenticated && (
        <Box p={5} shadow="md" boxSize="sm" borderRadius="md">
          <Flex justifyContent="center">
            <Image src={user?.picture} alt={user?.name} borderRadius="full" />
          </Flex>
          <Heading>{user?.name}</Heading>
          <Text>{user?.email}</Text>
        </Box>
      )}
    </>
  );
};

export default Profile;
